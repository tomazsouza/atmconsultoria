import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';

export default class BarraNavegacao extends Component {
    render() {
        return (
            <View style={styles.toolBar}>
                <Text style={styles.title}>ATM Consultoria</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    toolBar: {
        padding: 10,
        justifyContent: 'center',
        backgroundColor: '#CCC',
        height: 60
    },
    title: {
        flex: 1,
        fontSize: 18,
        textAlign: 'center',
        color: '#000'
    }
});