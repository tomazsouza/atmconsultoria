import React,  {Component} from 'react';
import {AppRegistry, View, StatusBar, StyleSheet, Image, TouchableOpacity} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flexDirection: 'row',
    },
    imagesIcon: {
        width: 100, height: 100,
        backgroundColor: '#888'
    },
    imagesGroup: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    logoPosition: {
        alignItems: 'center',
        marginTop: 8,
    }
});

const logo = require('../images/logo.png');
const menuClient = require('../images/menu_cliente.png');
const menuContact = require('../images/menu_contato.png');
const menuEnterprise = require('../images/menu_empresa.png');
const menuService = require('../images/menu_servico.png');

function nextSceneClient() {
}

export default class CenaPrincipal extends Component {
    render(): React.ReactNode {
        return (
            <View>
                <StatusBar backgroundColor='#ccc'/>
                <BarraNavegacao/>

                <View style={styles.logoPosition}>
                    <Image  source={logo}/>
                </View>

                <View>

                    <View style={styles.imagesGroup}>
                        <TouchableOpacity onPress={() => nextSceneClient}>
                            <Image style={styles.imagesIcon} source={menuClient}/>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => nextSceneClient}>
                            <Image style={styles.imagesIcon} source={menuContact}/>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.imagesGroup}>
                        <TouchableOpacity onPress={() => nextSceneClient}>
                            <Image style={styles.imagesIcon} source={menuEnterprise}/>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => nextSceneClient}>
                            <Image style={styles.imagesIcon} source={menuService}/>
                        </TouchableOpacity>
                    </View>

                </View>

            </View>
        );
    }
}
