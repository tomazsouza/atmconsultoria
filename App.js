import React, {Component} from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';

import CenaPrincipal from "./src/components/CenaPrincipal";

export default class App extends Component<Props> {
    render() {
        return (
            <View>
                <CenaPrincipal/>
            </View>
        );
    }
}
